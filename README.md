# Simple prohibited sites management application

Requires to have docker-compose installed.

Current coverage is 95%. Project was written in about 6 hours(to be more precise - 5:53). All the code in `apps`, `shared` folders was written during this time, didn't use any old one. Email service wasn't implemented due to the time lack(there is a dummy mechanics).

All commands below should be executed from project root, not `server` folder.

Prepare project to run:

```bash
make prepare
```

Run dev version([localhost:8080](http://localhost:8080/)):

```bash
make run
```

Has simple [Swagger UI here](http://localhost:8080/swagger/).

Run tests:

```bash
make test
```
