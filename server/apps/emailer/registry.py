from typing import *

__all__ = ('Registry', 'email_context_receivers_registry')


class Registry:
    """
    Context receivers registry. Not implemented.
    """
    receivers: dict

    def register(self, key: str, handler: Callable) -> Callable:
        return handler

    def get(self, key: str) -> Callable:
        return lambda **kwargs: {}


email_context_receivers_registry = Registry()
