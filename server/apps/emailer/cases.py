from typing import *

from .registry import email_context_receivers_registry


def send_email(
    event: str,
    recipients: Tuple[str],
    template_name: str,
    kwargs: dict = {},
    from_email: str=None,
):
    """
    Email sender mechanics. Not implemented.
    """
    context = email_context_receivers_registry.get(event)(**kwargs)
    # -> render template with context
    # -> send email with default send_email or other service
