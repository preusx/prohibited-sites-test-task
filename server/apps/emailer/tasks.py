from typing import *

from app.celery import app

from .cases import send_email


@app.task
def send_email_task(
    event: str,
    recipients: Tuple[str],
    template_name: str,
    kwargs: dict = {},
    from_email: str=None,
):
    send_email(
        event=event,
        from_email=from_email,
        recipients=recipients,
        kwargs=kwargs,
        template_name=template_name,
    )
