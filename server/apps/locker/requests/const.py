from django.db import models
from django.utils.translation import pgettext_lazy


class REQUEST_STATUSES(models.TextChoices):
    NEW = '0001000-new', pgettext_lazy('locker:requests', 'New')
    ACCEPTED = '0002000-accepted', pgettext_lazy('locker:requests', 'Accepted')
    REJECTED = '0003000-rejected', pgettext_lazy('locker:requests', 'Rejected')


RESOLVE_REQUEST_PERMISSION_NAME = 'resolve_request'
RESOLVE_REQUEST_PERMISSION = 'locker.' + RESOLVE_REQUEST_PERMISSION_NAME
