from django.db import models
from django.utils.translation import pgettext_lazy
from django.conf import settings

from apps.locker.blocks.validators import validate_domain
from .const import REQUEST_STATUSES, RESOLVE_REQUEST_PERMISSION_NAME

__all__ = ('Request',)


class Request(models.Model):
    STATUSES = REQUEST_STATUSES

    status = models.CharField(
        max_length=40, choices=REQUEST_STATUSES.choices,
        default=REQUEST_STATUSES.NEW
    )
    user_ip_address = models.GenericIPAddressField()
    user_email = models.EmailField()
    domain_name = models.CharField(
        max_length=512, unique=True, validators=(validate_domain,)
    )
    description = models.TextField()

    class Meta:
        verbose_name = pgettext_lazy('locker:requests', 'Block request')
        verbose_name_plural = pgettext_lazy('locker:requests', 'Block requests')
        permissions = (
            (
                RESOLVE_REQUEST_PERMISSION_NAME,
                pgettext_lazy('locker:requests', 'Block requests')
            ),
        )

    def __str__(self) -> str:
        return self.domain_name
