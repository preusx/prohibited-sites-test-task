from django.dispatch import Signal


request_status_changed = Signal(providing_args=['instance'])
request_resolved = Signal(providing_args=['instance'])
