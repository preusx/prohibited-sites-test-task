from rest_framework import serializers

from .models import Request

__all__ = ('RequestSerializer',)


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = (
            'id', 'status',
            'user_email', 'user_ip_address',
            'domain_name', 'description'
        )


class RequestCreateSerializer(RequestSerializer):
    class Meta(RequestSerializer.Meta):
        read_only_fields = ('status',)
