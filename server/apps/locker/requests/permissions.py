from rest_framework.permissions import DjangoModelPermissions

from .const import RESOLVE_REQUEST_PERMISSION


class CanResolveRequest(DjangoModelPermissions):
    PERM_LIST = [RESOLVE_REQUEST_PERMISSION]
    perms_map = {
        'GET': PERM_LIST, 'OPTIONS': PERM_LIST, 'HEAD': PERM_LIST,
        'POST': PERM_LIST, 'PUT': PERM_LIST, 'PATCH': PERM_LIST,
        'DELETE': PERM_LIST,
    }
