from apps.locker.blocks.cases import block_domain_ip

from .models import *
from .const import REQUEST_STATUSES
from .signals import request_status_changed, request_resolved


def set_request_status(request: Request, status: str):
    request.status = status
    request.save(update_fields=('status',))
    request_status_changed.send(sender=Request, instance=request)

    return request


def resolve_request(
    request: Request, status: str,
    domain_name: str=None, ip_address: str=None
) -> Request:
    request = set_request_status(request=request, status=status)

    if (
        REQUEST_STATUSES.ACCEPTED == status and
        (domain_name is not None and ip_address is not None)
    ):
        block_domain_ip(domain_name=domain_name, ip_address=ip_address)

    request_resolved.send(sender=Request, instance=request)

    return request
