from django.dispatch import receiver
from django.conf import settings

from apps.emailer.tasks import send_email_task
from apps.locker.requests.signals import request_resolved

from .email_events import (
    REQUEST_RESOLVED_EMAIL_EVENT, REQUEST_RESOLVED_EMAIL_TEMPLATE
)


@receiver(request_resolved)
def request_resolved_email_notify_subscriber(sender, instance, **kwargs):
    send_email_task.delay(
        event=REQUEST_RESOLVED_EMAIL_EVENT,
        recipients=[instance.user_email],
        kwargs={'request_id': instance.id},
        template_name=REQUEST_RESOLVED_EMAIL_TEMPLATE,
    )
