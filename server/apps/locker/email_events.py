from django.conf import settings
from apps.emailer import email_context_receivers_registry


REQUEST_RESOLVED_EMAIL_EVENT = 'REQUEST_RESOLVED_EMAIL_NOTIFY'
REQUEST_RESOLVED_EMAIL_TEMPLATE = getattr(
    settings, 'LOCKER_EMAIL_REQUEST_RESOLVED_TEMPLATE', 'some/template/path.html'
)


def request_resolved_email_notify_context_receiver(request_id, **kwargs):
    # Email context data receiver. Not implemented.
    return {'request_id': request_id}


email_context_receivers_registry.register(
    REQUEST_RESOLVED_EMAIL_EVENT,
    request_resolved_email_notify_context_receiver
)
