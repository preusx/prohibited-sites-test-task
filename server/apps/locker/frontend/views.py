from rest_framework import generics
from rest_framework.permissions import AllowAny

from shared.permissions import DjangoModelPermissions
from apps.locker.requests.models import Request
from apps.locker.requests.serializers import (
    RequestSerializer, RequestCreateSerializer
)
from apps.locker.blocks.models import BlockedDomain, BlockedIP
from apps.locker.blocks.serializers import (
    BlockedDomainSerializer, BlockedIPSerializer, BlockDomainIPSerializer
)
from apps.locker.requests.permissions import CanResolveRequest
from .serializers import BlockedIPListSerializer, RequestResolveSerializer
from .receivers import receive_user_ip

__all__ = (
    'BlockedDomainListAPIView', 'BlockedIPListAPIView', 'BlockedIPBlockAPIView',
    'RequestListAPIView', 'RequestCreateAPIView', 'RequestResolveAPIView'
)


class BlockedDomainListAPIView(generics.ListAPIView):
    queryset = BlockedDomain.objects.all()
    serializer_class = BlockedDomainSerializer
    permission_classes = (DjangoModelPermissions,)


class BlockedIPListAPIView(generics.ListAPIView):
    queryset = BlockedIP.objects.select_related('domain')
    serializer_class = BlockedIPListSerializer
    permission_classes = (DjangoModelPermissions,)


class BlockedIPBlockAPIView(generics.CreateAPIView):
    queryset = BlockedIP.objects.all()
    serializer_class = BlockDomainIPSerializer
    permission_classes = (DjangoModelPermissions,)


class RequestListAPIView(generics.ListAPIView):
    queryset = Request.objects.all()
    permission_classes = (CanResolveRequest,)
    serializer_class = RequestSerializer


class RequestCreateAPIView(generics.CreateAPIView):
    queryset = Request.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RequestCreateSerializer

    def get_serializer(self, *args, data=None, **kwargs):
        data = (data or {}).copy()
        data['user_ip_address'] = receive_user_ip(self.request)

        return super().get_serializer(*args, data=data, **kwargs)


class RequestResolveAPIView(generics.UpdateAPIView):
    http_method_names = ('put',)
    queryset = Request.objects.filter(status=Request.STATUSES.NEW)
    permission_classes = (CanResolveRequest,)
    serializer_class = RequestResolveSerializer
