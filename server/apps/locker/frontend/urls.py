from django.urls import path, include
from . import views


app_name = 'locker'

block_domains_api = (
    path('list/', views.BlockedDomainListAPIView.as_view(), name='list'),
)

block_ips_api = (
    path('list/', views.BlockedIPListAPIView.as_view(), name='list'),
    path('block/', views.BlockedIPBlockAPIView.as_view(), name='block'),
)

requests_api = (
    path('list/', views.RequestListAPIView.as_view(), name='list'),
    path('create/', views.RequestCreateAPIView.as_view(), name='create'),
    path('<int:pk>/resolve/', views.RequestResolveAPIView.as_view(), name='resolve'),
)

api = (
    path('domains/', include((block_domains_api, app_name), 'domains')),
    path('ips/', include((block_ips_api, app_name), 'ips')),
    path('requests/', include((requests_api, app_name), 'requests')),
)

urlpatterns = (
    path('api/v1/locker/', include((api, app_name), 'api')),
)
