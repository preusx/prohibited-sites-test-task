from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class LockerFrontendConfig(AppConfig):
    name = 'apps.locker.frontend'
    label = 'locker_frontend'
    verbose_name = pgettext_lazy('locker:frontend:app', 'Locker frontend')
