def receive_user_ip(request):
    forwarded = request.META.get('HTTP_X_FORWARDED_FOR')
    if forwarded:
        return forwarded.split(',')[0].strip()

    real = request.META.get('HTTP_X_REAL_IP')
    if real:
        return real

    return request.META.get('REMOTE_ADDR')
