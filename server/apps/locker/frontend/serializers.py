from rest_framework import serializers

from apps.locker.blocks.serializers import (
    BlockedDomainSerializer, BlockedIPSerializer, BlockDomainIPSerializer
)
from apps.locker.requests.models import Request
from apps.locker.requests.cases import resolve_request


class BlockedIPListSerializer(BlockedIPSerializer):
    domain = BlockedDomainSerializer()


class RequestResolveSerializer(serializers.ModelSerializer):
    domain_info = BlockDomainIPSerializer(
        allow_null=True, write_only=True, required=False
    )

    class Meta:
        model = Request
        fields = ('id', 'status', 'domain_info')

    def update(self, instance, data):
        return resolve_request(
            instance, status=data['status'], **(data.get('domain_info') or {})
        )
