from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase, APITransactionTestCase
from rest_framework import status

from shared.testing import catch_signal
from apps.locker.requests.models import Request
from apps.locker.blocks.models import BlockedDomain, BlockedIP
from apps.locker.requests.signals import request_status_changed, request_resolved
from apps.locker.blocks.signals import domain_ip_blocked


USER_IP = '127.0.0.2'
USER_EMAIL = 'ad@ad.ad'


def create_request(
    status=Request.STATUSES.NEW,
    user_ip_address=USER_IP,
    user_email=USER_EMAIL,
    description='Why not?',
    **kwargs
):
    return Request.objects.create(
        status=status,
        user_ip_address=user_ip_address,
        user_email=user_email,
        description=description,
        **kwargs
    )


class AccessRequestApiTests(APITestCase):
    def setUp(self):
        self.list_url = '/api/v1/locker/requests/list/'
        self.reslove_url = lambda x: f'/api/v1/locker/requests/{x}/resolve/'

    def tearDown(self):
        Request.objects.all().delete()

    def test_list_returns_403_for_not_unauthorized(self):
        block_request = create_request(domain_name='google.com')
        res = self.client.get(self.list_url)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_resolve_returns_403_for_not_unauthorized(self):
        res = self.client.put(self.reslove_url(5), data={})
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)


class SendRequestApiTests(APITransactionTestCase):
    def setUp(self):
        self.create_url = '/api/v1/locker/requests/create/'

    def test_create_block_request(self):
        data = {
            'domain_name': 'google.com',
            'user_email': 'some@email.com',
            'description': 'Spreading COVID-19 through WI-FI',
            'status': Request.STATUSES.ACCEPTED
        }
        res = self.client.post(self.create_url, data=data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.json()['domain_name'], data['domain_name'])
        self.assertEqual(res.json()['status'], Request.STATUSES.NEW)

    def test_create_invalid_block_request(self):
        data = {
            'domain_name': 'other thing here',
            'user_email': '@email.com',
            'description': ''
        }
        res = self.client.post(self.create_url, data=data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        json = res.json()
        self.assertIn('description', json)
        self.assertIn('user_email', json)
        self.assertIn('domain_name', json)


class ResolveRequestApiTests(APITransactionTestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_superuser(
            username='superadmin', password='11'
        )
        self.reslove_url = lambda x: f'/api/v1/locker/requests/{x}/resolve/'
        self.client.force_authenticate(user=self.user)

    def test_reject_block_request(self):
        request = create_request(domain_name='google.com')
        data = {'status': Request.STATUSES.REJECTED}
        res = self.client.put(self.reslove_url(request.pk), data=data)
        request.refresh_from_db()

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(request.status, Request.STATUSES.REJECTED)

    def test_reject_non_new_block_request(self):
        request = create_request(
            domain_name='google.com', status=Request.STATUSES.ACCEPTED
        )
        data = {'status': Request.STATUSES.REJECTED}
        res = self.client.put(self.reslove_url(request.pk), data=data)
        request.refresh_from_db()

        self.assertEqual(res.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(request.status, Request.STATUSES.ACCEPTED)

    def test_accept_empty_block_request(self):
        request = create_request(domain_name='google.com')
        data = {'status': Request.STATUSES.ACCEPTED}
        res = self.client.put(self.reslove_url(request.pk), data=data)
        request.refresh_from_db()

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(request.status, Request.STATUSES.ACCEPTED)
        self.assertEqual(BlockedIP.objects.count(), 0)

    def test_accept_block_request_with_locking(self):
        request = create_request(domain_name='google.com')
        data = {
            'status': Request.STATUSES.ACCEPTED,
            'domain_info': {
                'domain_name': 'google.com',
                'ip_address': '120.20.20.20'
            }
        }
        res = self.client.put(self.reslove_url(request.pk), data=data, format='json')
        request.refresh_from_db()

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(request.status, Request.STATUSES.ACCEPTED)
        self.assertEqual(BlockedIP.objects.count(), 1)
        self.assertEqual(BlockedDomain.objects.count(), 1)

    def test_accept_block_request_with_locking_invalid(self):
        request = create_request(domain_name='google.com')
        data = {
            'status': Request.STATUSES.ACCEPTED,
            'domain_info': {
                'domain_name': 'ss google com',
                'ip_address': '10.120.20.20.20'
            }
        }
        res = self.client.put(self.reslove_url(request.pk), data=data, format='json')
        json = res.json()

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('domain_info', json)
        self.assertIn('domain_name', json['domain_info'])
        self.assertIn('ip_address', json['domain_info'])


class ResolveRequestSignalTests(APITransactionTestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_superuser(
            username='superadmin', password='11'
        )
        self.reslove_url = lambda x: f'/api/v1/locker/requests/{x}/resolve/'
        self.client.force_authenticate(user=self.user)

    def test_reject_block_request(self):
        request = create_request(domain_name='google.com')
        data = {'status': Request.STATUSES.REJECTED}

        with catch_signal(request_status_changed) as handler:
            self.client.put(self.reslove_url(request.pk), data=data)
            handler.assert_called()

    def test_accept_empty_block_request(self):
        request = create_request(domain_name='google.com')
        data = {'status': Request.STATUSES.ACCEPTED}

        with catch_signal(request_status_changed) as handler:
            with catch_signal(request_resolved) as handler_resolver:
                with catch_signal(domain_ip_blocked) as handler_blocker:
                    self.client.put(self.reslove_url(request.pk), data=data)
                    handler_blocker.assert_not_called()
                handler_resolver.assert_called()
            handler.assert_called()

    def test_accept_block_request_with_locking(self):
        request = create_request(domain_name='google.com')
        data = {
            'status': Request.STATUSES.ACCEPTED,
            'domain_info': {
                'domain_name': 'google.com',
                'ip_address': '120.20.20.20'
            }
        }
        with catch_signal(request_status_changed) as handler:
            with catch_signal(request_resolved) as handler_resolver:
                with catch_signal(domain_ip_blocked) as handler_blocker:
                    self.client.put(self.reslove_url(request.pk), data=data, format='json')
                    handler_blocker.assert_called()
                handler_resolver.assert_called()
            handler.assert_called()
