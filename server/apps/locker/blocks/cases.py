from .models import *
from .signals import domain_ip_blocked


def block_domain_ip(domain_name: str, ip_address: str) -> BlockedIP:
    domain, created = BlockedDomain.objects.get_or_create(domain_name=domain_name)
    result = BlockedIP.objects.create(domain=domain, ip_address=ip_address)

    domain_ip_blocked.send(sender=BlockedIP, blocked_ip=result)

    return result
