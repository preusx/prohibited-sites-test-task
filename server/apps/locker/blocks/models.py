from django.db import models
from django.utils.translation import pgettext_lazy
from django.conf import settings

from .validators import validate_domain


__all__ = ('BlockedDomain', 'BlockedIP',)


class BlockedDomain(models.Model):
    domain_name = models.CharField(
        max_length=512, unique=True, validators=(validate_domain,)
    )

    class Meta:
        verbose_name = pgettext_lazy('locker:blocks', 'Заблокированный сайт')
        verbose_name_plural = pgettext_lazy('locker:blocks', 'Заблокированные сайт')

    def __str__(self) -> str:
        return self.domain_name


class BlockedIP(models.Model):
    ip_address = models.GenericIPAddressField(unique=True)
    domain = models.ForeignKey(BlockedDomain, on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext_lazy('locker:blocks', 'Заблокированный IP')
        verbose_name_plural = pgettext_lazy('locker:blocks', 'Заблокированные IP')

    def __str__(self) -> str:
        return self.ip_address
