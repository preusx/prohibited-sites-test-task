from django.contrib import admin

from .models import *


admin.site.register(BlockedDomain)
admin.site.register(BlockedIP)
