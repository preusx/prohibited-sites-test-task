from rest_framework import serializers

from .models import BlockedDomain, BlockedIP
from .validators import validate_domain
from .cases import block_domain_ip

__all__ = ('RequestSerializer',)


class BlockedDomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlockedDomain
        fields = ('id', 'domain_name')


class BlockedIPSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlockedIP
        fields = ('id', 'ip_address', 'domain')


class BlockDomainIPSerializer(serializers.ModelSerializer):
    domain_name = serializers.CharField(
        validators=(validate_domain,), write_only=True
    )

    class Meta:
        model = BlockedIP
        fields = ('id', 'ip_address', 'domain_name')

    def create(self, data):
        return block_domain_ip(
            domain_name=data['domain_name'],
            ip_address=data['ip_address']
        )
