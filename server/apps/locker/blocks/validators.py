import re

from django.utils.translation import pgettext_lazy
from django.core.validators import RegexValidator, URLValidator


validate_domain = RegexValidator(
    regex=URLValidator.host_re,
    flags=re.IGNORECASE,
    message = pgettext_lazy('locker:blocks', 'Enter a valid domain.')
)
