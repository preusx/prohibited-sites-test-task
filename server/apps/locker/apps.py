from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class LockerConfig(AppConfig):
    name = 'apps.locker'
    verbose_name = pgettext_lazy('locker:app', 'Locker')

    def ready(self):
        from . import subscriptions
