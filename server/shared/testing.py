from unittest import mock
from contextlib import contextmanager


@contextmanager
def catch_signal(signal):
    handler = mock.Mock()

    signal.connect(handler)
    yield handler
    signal.disconnect(handler)
