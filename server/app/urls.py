from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib.auth.views import LogoutView
from django.contrib import admin
from django.contrib.staticfiles.urls import static
from django.urls import path

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^django-des/', include('des.urls')),
    url(r'^api/v1/auth/', include('rest_auth.urls')),

    url(r'^', include('apps.locker.frontend.urls')),
]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^rosetta/', include('rosetta.urls'))]

if 'drf_yasg' in  settings.INSTALLED_APPS:
    from rest_framework import permissions
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title='API',
            default_version='v1',
            description='Test description',
            terms_of_service='https://www.google.com/policies/terms/',
            contact=openapi.Contact(email='contact@snippets.local'),
            license=openapi.License(name='BSD License'),
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )
    urlpatterns += [
        url(
            r'^swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0),
            name='schema-json',
        ),
        url(
            r'^swagger/$',
            schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui',
        ),
    ]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
