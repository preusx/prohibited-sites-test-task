prepare:
	cp ./server/.env_template ./server/.env
	cp ./server/app/settings/local_template.py ./server/app/settings/local.py

run:
	docker-compose up

test:
	docker-compose run backend /bin/bash -c "pip install coverage && coverage run --source='.' manage.py test --noinput && coverage report"
